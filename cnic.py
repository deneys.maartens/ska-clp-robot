from robot.api.deco import keyword


class cnic:
    @keyword('Clear the CNIC signal for station ${station}')
    def clear_signal(self, station):
        print(f'Outputing 0V DC on station {station}')

    @keyword('Add a ${frequency}MHz CW signal with amplitude'
             ' ${amplitude}V to the CNIC for station ${station}')
    def generate_cw_signal(self, frequency, amplitude, station):
        print(f'Generating {frequency}MHz cosine of'
              ' {amplitude}V on station {station}')


if __name__ == '__main__':
    c = cnic()
    c.clear_signal('X')
