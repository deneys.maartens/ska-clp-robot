from robot.api.deco import keyword


class sdp_sim:
    @keyword('In SDP Configure boresight delay polynomial server')
    def configure_boresight(self):
        print('Starting HTTP server...')
        print('Done.')

    @keyword('In SDP Inspect ${nchan} visibility correlation products')
    def inspect_correlation_products(self, nchan):
        print(f'Inspecting {nchan} visibility correlation products')

    @keyword('In SDP Expect spectral line emission in ${channel} channel')
    def probe_spectral_line(self, channel):
        print(f'Looking for emission in {channel} channel')
