from robot.api.deco import keyword


class csp_low_lmc:
    @keyword('CSP-Low LMC Configure scan')
    def scan_configure(self):
        print('Configuring scan; sending json to csp_low_lmc')

    @keyword('CSP-Low LMC Execute scan')
    def scan_execute(self):
        print('Executing scan; sending json to csp_low_lmc')

    @keyword('CSP-Low LMC End scan')
    def scan_end(self):
        print('End of scan; sending json to csp_low_lmc')

    @keyword('CSP-Low LMC Release resources')
    def resources_release(self):
        print('Resources released; sending json to csp_low_lmc')

# vim:set norelativenumber:set nospell:
