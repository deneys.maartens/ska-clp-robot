*** Settings ***
Documentation    As a radio astronomer
...              I want to do spectral line observations
...              so that I can probe the physical conditions of celestial objects
Resource         spectral_line_observations.resource

*** Test Cases ***
Spectral line at the centre of the band
    Given an object with spectral line emission at 200MHz
    When correlating the signals
    then the 16 visibility correlation products show a spectral line emission in the centre channel

Spectral line at the lower edge of the band
    Given an object with spectral line emission at 50Mhz
    When correlating the signals
    then the 16 visibility correlation products show a spectral line emission in the leftmost channel

Spectral line at the upper edge of the band
    Given an object with spectral line emission at 350Mhz
    When correlating the signals
    then the 16 visibility correlation products show a spectral line emission in the rightmost channel
