# Feature: Spectral line observations

As a radio astronomer  
I want to do spectral line observations,  
so that I can probe the physical conditions of celestial objects.

## Rule: A spectral line at a known frequency is detected in the fine channel associated with that frequency

In interferometry channelisation is used to pinpoint the received frequencies. Depending on the configured
mode the full frequency band from 50MHz to 350MHz is divided into a fixed number of channels, all with
the same bandwidth. Increasing the amount of channels increases the frequency resolution. To achieve the
required resolution for SKA-Low there will be up to approximately 64000 channels.

![channelisation](channel_sketch.jpg)

Background:

Given a single baseline between 2 dual pole station beams  
and the correlator is configured in 300Mhz continuum mode

Example: Spectral line at the centre of the band

Given an object with spectral line emission at 200Mhz  
when correlating the signals  
then the 16 visibility correlation products show a spectral line emission in the centre channel

Example: Spectral line at the lower edge of the band

Given an object with spectral line emission at 50Mhz  
When correlating the signals  
then the 16 visibility correlation products show a spectral line emission in the leftmost channel

Example: Spectral line at the upper edge of the band

Given an object with spectral line emission at 350Mhz  
When correlating the signals  
then the 16 visibility correlation products show a spectral line emission in the rightmost channel

## Rule: The amplitude of the auto correlation product must correspond to the amplitude of the input signal

## Rule: Sources with the same brightness show the same power on all frequencies
